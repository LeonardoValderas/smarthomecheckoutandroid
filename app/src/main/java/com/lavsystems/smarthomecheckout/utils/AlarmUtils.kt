package com.lavsystems.smarthomecheckout.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.view.service.AlarmStartBroadcastReceiver
import com.lavsystems.smarthomecheckout.mvvm.view.service.AlarmStopBroadcastReceiver
import java.util.*

object AlarmUtils {
    private val REQUEST_CODE_START = 0
    private val REQUEST_CODE_STOP = 1

    fun cancelAlarm(context: Context) {
        val alarmManagerStart = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentStart = getPendingIntentStartAlarm(context)
        alarmManagerStart.cancel(pendingIntentStart)

        val alarmManagerStop = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentStop = getPendingIntentStopAlarm(context)
        alarmManagerStop.cancel(pendingIntentStop)
    }

    fun initAlarm(alarm: Alarm, context: Context) {
        try {
            val alarmManagerStart = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pendingIntentStart = getPendingIntentStartAlarm(context)
            val calendarStart = getCalendar(alarm.startHour.hourInt)

            alarmManagerStart.setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendarStart.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentStart
            )

            val alarmManagerStop = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pendingIntentStop = getPendingIntentStopAlarm(context)
            val calendarStop = getCalendar(alarm.stopHour.hourInt)
            alarmManagerStop.setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendarStop.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentStop
            )

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getCalendar(hour: Int): Calendar {
        val now = Calendar.getInstance()
        val calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            this[Calendar.HOUR_OF_DAY] = hour
            this[Calendar.MINUTE] = 0
            this[Calendar.SECOND] = 0
        }.also {
            if(it.before(now)){
                it.add(Calendar.DATE, 1);
            }
        }

        return calendar
    }

    private fun getPendingIntentStartAlarm(context: Context): PendingIntent{
        val intentAlarmStart = Intent(context, AlarmStartBroadcastReceiver::class.java)
        return PendingIntent.getBroadcast(
            context,
            REQUEST_CODE_START,
            intentAlarmStart,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun getPendingIntentStopAlarm(context: Context): PendingIntent{
        val intentAlarmStop = Intent(context, AlarmStopBroadcastReceiver::class.java)
        return PendingIntent.getBroadcast(
            context,
            REQUEST_CODE_STOP,
            intentAlarmStop,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun alarmStartIsUp(context: Context): Boolean{
        val intentAlarmStart = Intent(context, AlarmStartBroadcastReceiver::class.java)
        return (PendingIntent.getBroadcast(
            context,
            REQUEST_CODE_START,
            intentAlarmStart,
            PendingIntent.FLAG_NO_CREATE) != null
        )
    }

    private fun alarmStopIsUp(context: Context): Boolean {
        val intentAlarmStop = Intent(context, AlarmStopBroadcastReceiver::class.java)
        return (PendingIntent.getBroadcast(
            context,
            REQUEST_CODE_STOP,
            intentAlarmStop,
            PendingIntent.FLAG_NO_CREATE) != null
        )
    }
}