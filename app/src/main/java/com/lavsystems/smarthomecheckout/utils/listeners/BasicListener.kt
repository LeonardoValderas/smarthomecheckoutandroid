package com.lavsystems.smarthomecheckout.utils.listeners

interface BasicListener<T> {
    fun onStart()
    fun onSuccess(data: T)
    fun onFailed(e: Exception)
}