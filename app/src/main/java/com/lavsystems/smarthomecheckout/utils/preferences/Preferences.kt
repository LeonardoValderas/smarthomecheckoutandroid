package com.lavsystems.smarthomecheckout.utils.preferences

import com.lavsystems.smarthomecheckout.mvvm.model.Alarm

interface Preferences {
    fun saveDataBaseName(name: String): Boolean
    fun getDataBaseName(): String?
    fun removeDataBaseName(): Boolean
    fun saveLastDate(date: String): Boolean
    fun getLastDate(): String?
    fun removeLastDate(): Boolean
    fun saveAlarm(alarm: Alarm): Boolean
    fun getAlarm(): Alarm?
}