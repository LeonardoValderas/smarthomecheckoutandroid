package com.lavsystems.smarthomecheckout.utils.listeners

interface LoginListener<T> {
    fun onStart()
    fun onSuccess(data: T)
    fun onFailed(e: Exception)
}