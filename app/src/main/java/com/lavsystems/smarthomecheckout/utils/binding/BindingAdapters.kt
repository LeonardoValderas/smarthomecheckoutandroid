package com.lavsystems.smarthomecheckout.utils.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.lavsystems.smarthomecheckout.R

object BindingAdapters{
    @BindingAdapter("iconResource", "imageResource")
    @JvmStatic
    fun loadImageFromResource(view: ImageView, resource: String, placeHolder: Int) {
        val context = view.context
        val id = context.resources.getIdentifier(resource, "drawable", context.packageName)
        Glide.with(context)
            .load(id)
            .placeholder(placeHolder)
            .override(50, 50)
            .dontAnimate()
            .error(R.drawable.ic_error_image)
            .into(view)
    }
}