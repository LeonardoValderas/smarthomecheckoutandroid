package com.lavsystems.smarthomecheckout.utils

object ConstantsUtils {
    const val FIREBASE_BASE_NAME = "name_base"
    const val FIREBASE_CUSTOMERS = "customers"
    const val FIREBASE_ID = "id"
    const val FIREBASE_EVENT_FOUND = "event_found"
    const val FIREBASE_ACTIVE = "active"



    const val FIREBASE_LAST_DATE = "last_update"
    const val FIREBASE_ALARM = "alarma"
    const val FIREBASE_ALARM_START = "start"
    const val FIREBASE_ALARM_STOP = "sport"
    const val FIREBASE_ALARM_ACTIVE = "active"
    const val FIREBASE_COMPONENTS = "components"

    const val CHANNEL_ID = "channel_id_notification"
    const val CHANNEL_ID_NAME = "Smart Home Notificación"
}