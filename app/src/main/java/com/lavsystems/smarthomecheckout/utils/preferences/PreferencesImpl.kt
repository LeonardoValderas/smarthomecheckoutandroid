package com.lavsystems.smarthomecheckout.utils.preferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm

class PreferencesImpl(private val context: Context) : Preferences {
    companion object {
        const val SHARED_PREFERENCES_SMART_HOME = "smart_home_preferences"
        const val SHARED_PREFERENCES_ALARM_KEY = "alarm_key"
        const val SHARED_PREFERENCES_DATABASE_NAME_KEY = "database_name_key"
        const val SHARED_PREFERENCES_LAST_DATE_KEY = "last_date_key"
    }

    override fun saveDataBaseName(name: String): Boolean {
        return getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).edit().apply {
            putString(SHARED_PREFERENCES_DATABASE_NAME_KEY, name)
        }.commit()
    }

    override fun getDataBaseName(): String? {
        return getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).getString(
            SHARED_PREFERENCES_DATABASE_NAME_KEY,
            ""
        )
    }

    override fun removeDataBaseName(): Boolean {
        return getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).edit().apply {
            putString(SHARED_PREFERENCES_DATABASE_NAME_KEY, "")
        }.commit()
    }

    override fun saveLastDate(date: String): Boolean {
        return getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).edit().apply {
            putString(SHARED_PREFERENCES_LAST_DATE_KEY, date)
        }.commit()
    }

    override fun getLastDate(): String? {
        return getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).getString(
            SHARED_PREFERENCES_LAST_DATE_KEY,
            ""
        )
    }

    override fun removeLastDate(): Boolean {
        return getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).edit().apply {
            putString(SHARED_PREFERENCES_LAST_DATE_KEY, "")
        }.commit()
    }

    override fun saveAlarm(alarm: Alarm): Boolean {
        val alarmToJson = Gson().toJson(alarm)
        return getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).edit().apply {
            putString(SHARED_PREFERENCES_ALARM_KEY, alarmToJson)
        }.commit()
    }

    override fun getAlarm(): Alarm? {
        getSharedPreferences(SHARED_PREFERENCES_SMART_HOME).getString(
            SHARED_PREFERENCES_ALARM_KEY,
            ""
        )?.let { a ->
            try {
                a.let {
                    return Gson().fromJson(it, Alarm::class.java)
                }
            } catch (e: Exception) {
                return null
            } catch (e: JsonSyntaxException) {
                return null
            }
        }

        return null
    }

    private fun getSharedPreferences(key: String): SharedPreferences {
        return context.getSharedPreferences(key, Context.MODE_PRIVATE)
    }
}