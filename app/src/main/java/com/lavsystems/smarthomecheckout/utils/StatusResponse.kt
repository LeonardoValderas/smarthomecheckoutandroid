package com.lavsystems.smarthomecheckout.utils

enum class StatusResponse {
    SUCCESS,
    LOADING,
    ERROR
}