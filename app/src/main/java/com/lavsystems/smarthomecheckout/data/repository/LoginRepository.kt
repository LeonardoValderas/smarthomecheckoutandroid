package com.lavsystems.smarthomecheckout.data.repository

import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

interface LoginRepository {
    fun validateCustomerId(id: String, listener: BasicListener<Boolean>)
}