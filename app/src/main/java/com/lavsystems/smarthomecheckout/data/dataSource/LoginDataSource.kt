package com.lavsystems.smarthomecheckout.data.dataSource

import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

interface LoginDataSource {
    fun validateCustomerId(id: String, listener: BasicListener<Boolean>)
}