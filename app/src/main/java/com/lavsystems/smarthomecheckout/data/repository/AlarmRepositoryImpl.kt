package com.lavsystems.smarthomecheckout.data.repository

import com.lavsystems.smarthomecheckout.data.dataSource.AlarmDataSource
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class AlarmRepositoryImpl(private val dataSource: AlarmDataSource): AlarmRepository {
    override fun saveAlarm(alarm: Alarm, listener: BasicListener<Boolean>) {
        return dataSource.saveAlarm(alarm, listener)
    }

    override fun activateOrDesactivateAlarm(newStatus: Boolean, listener: BasicListener<Boolean>) {
        return dataSource.activateOrDesactivateAlarm(newStatus, listener)
    }

    override fun getAlarmDataFromPreferences(listener: BasicListener<Alarm>) {
        return dataSource.getAlarmDataFromPreferences(listener)
    }
    override fun getAlarmData(listener: BasicListener<Alarm>) {
        return dataSource.getAlarmData(listener)
    }
}