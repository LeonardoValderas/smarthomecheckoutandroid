package com.lavsystems.smarthomecheckout.data.dataSource

import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener
import com.lavsystems.smarthomecheckout.utils.listeners.LoginListener

class LoginDataSourceImpl(private val firebaseManager: FirebaseManager) : LoginDataSource {
    override fun validateCustomerId(id: String, listener: BasicListener<Boolean>){
        return firebaseManager.validateCustomerId(id, listener)
    }
}