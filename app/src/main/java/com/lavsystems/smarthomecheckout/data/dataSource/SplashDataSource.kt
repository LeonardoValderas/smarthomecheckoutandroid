package com.lavsystems.smarthomecheckout.data.dataSource

interface SplashDataSource {
    fun baseNameExists(): Boolean
}