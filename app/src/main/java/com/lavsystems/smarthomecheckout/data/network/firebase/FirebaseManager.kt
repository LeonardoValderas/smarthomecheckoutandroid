package com.lavsystems.smarthomecheckout.data.network.firebase

import android.nfc.NfcAdapter
import com.google.firebase.database.*
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_ACTIVE
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_ALARM
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_ALARM_ACTIVE
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_ALARM_START
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_ALARM_STOP
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_BASE_NAME
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_COMPONENTS
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_CUSTOMERS
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_EVENT_FOUND
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_ID
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.FIREBASE_LAST_DATE
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener
import com.lavsystems.smarthomecheckout.utils.preferences.Preferences
import org.koin.ext.scope
import org.koin.java.KoinJavaComponent.inject

class FirebaseManager {
    val preferences by inject(Preferences::class.java)

    fun validateCustomerId(id: String, listener: BasicListener<Boolean>) {
        listener.onStart()
        val firebase = getDatabaseReference(null)
        try {
            firebase
                .child(FIREBASE_CUSTOMERS)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {
                        listener.onFailed(error.toException())
                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (snapshot.exists()) {
                            for (s: DataSnapshot in snapshot.children) {
                                s.child(FIREBASE_ID).value?.let { v ->
                                    if (v == id) {
                                        val baseName = s.child(FIREBASE_BASE_NAME).value
                                        baseName?.let {
                                            preferences.saveDataBaseName(baseName as String)
                                            listener.onSuccess(true)
                                            return
                                        } ?: kotlin.run {
                                            listener.onSuccess(false)
                                            return
                                        }
                                    }
                                }
                            }
                            listener.onSuccess(false)
                        }
                    }
                })
        } catch (e: Exception) {
            listener.onFailed(e)
        }
    }

    fun exists(): Boolean {
        val baseName = preferences.getDataBaseName()
        return !baseName.isNullOrEmpty()
    }

    fun getComponents(removedListener: Boolean, listener: BasicListener<MutableList<Component>>) {
        val valueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                listener.onFailed(error.toException())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val components = mutableListOf<Component>()
                if (snapshot.exists()) {
                    for (s: DataSnapshot in snapshot.children) {
                        val c = s.getValue(Component::class.java)
                        c?.let {
                            components.add(it)
                        }
                    }
                    listener.onSuccess(components)
                    return
                }
                listener.onSuccess(mutableListOf())
            }
        }

        listener.onStart()
        val firebase = getDatabaseReference(null)
        getDatabaseName()?.let { name ->
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_COMPONENTS)
                    .addValueEventListener(valueEventListener)
            } catch (e: Exception) {
                listener.onFailed(e)
            }

        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
//        if(removedListener)
//            firebase
////                .child(name)
////                .child(FIREBASE_COMPONENTS)
//                .removeEventListener(valueEventListener)
    }

    fun getComponentsOnceTime(listener: BasicListener<MutableList<Component>>) {
        val valueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                listener.onFailed(error.toException())
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val components = mutableListOf<Component>()
                if (snapshot.exists()) {
                    for (s: DataSnapshot in snapshot.children) {
                        val c = s.getValue(Component::class.java)
                        c?.let {
                            components.add(it)
                        }
                    }
                    listener.onSuccess(components)
                    return
                }
                listener.onSuccess(mutableListOf())
            }
        }

        listener.onStart()
        val firebase = getDatabaseReference(null)
        getDatabaseName()?.let { name ->
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_COMPONENTS)
                    .addListenerForSingleValueEvent(valueEventListener)
            } catch (e: Exception) {
                listener.onFailed(e)
            }

        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
//            firebase
//                .child(name)
//                .child(FIREBASE_COMPONENTS)
//                .removeEventListener(valueEventListener)
    }

    fun lastDateChanged(listener: BasicListener<Boolean>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_LAST_DATE)
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {
                            listener.onFailed(error.toException())
                        }

                        override fun onDataChange(snapshot: DataSnapshot) {
                            snapshot.value?.let { date ->
                                getLastDate()?.let {
                                    if (it.isEmpty()) {
                                        preferences.saveLastDate(date as String)
                                        listener.onSuccess(true)
                                        return
                                    } else {
                                        val equals = (date as String) != it
                                        listener.onSuccess(equals)
                                        return
                                    }
                                }
                            }
                            listener.onSuccess(true)
                        }
                    })
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun resetEvent(componentName: String, listener: BasicListener<Boolean>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_COMPONENTS)
                    .child(componentName)
                    .child(FIREBASE_EVENT_FOUND).setValue(0)
                listener.onSuccess(true)
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun activateOrDesactivate(componentName: String, newStatus: Int, listener: BasicListener<Boolean>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_COMPONENTS)
                    .child(componentName)
                    .child(FIREBASE_ACTIVE).setValue(newStatus)
                listener.onSuccess(true)
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun activateOrDesactivateComponents(components: MutableList<Component>, listener: BasicListener<Boolean>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_COMPONENTS)
                    .setValue(components)
                listener.onSuccess(true)
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun activateOrDesactivateAlarm(newStatus: Boolean, listener: BasicListener<Boolean>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_ALARM)
                    .child(FIREBASE_ALARM_ACTIVE).setValue(newStatus)
                listener.onSuccess(true)
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun saveAlarm(alarm: Alarm, listener: BasicListener<Boolean>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_ALARM).setValue(alarm)
                listener.onSuccess(true)
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun getAlarmData(listener: BasicListener<Alarm>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_ALARM)
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {
                            listener.onFailed(error.toException())
                        }

                        override fun onDataChange(snapshot: DataSnapshot) {
                            val alarm = snapshot.getValue(Alarm::class.java)
                             alarm?.let {
                                 preferences.saveAlarm(it)
                                 listener.onSuccess(it)
                             }
                        }
                    })
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun alarmIsActive(listener: BasicListener<Boolean>) {
        listener.onStart()
        getDatabaseName()?.let { name ->
            val firebase = getDatabaseReference(null)
            try {
                firebase
                    .child(name)
                    .child(FIREBASE_ALARM)
                    .child(FIREBASE_ALARM_ACTIVE)
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {
                            listener.onFailed(error.toException())
                        }

                        override fun onDataChange(snapshot: DataSnapshot) {
                            val isActive = snapshot.value as Boolean
                            listener.onSuccess(isActive)
//                            alarm?.let {
//                                preferences.saveAlarm(it)
//                                listener.onSuccess(it)
//                            }
                        }
                    })
            } catch (e: Exception) {
                listener.onFailed(e)
            }
        } ?: kotlin.run {
            listener.onFailed(Exception("null value"))
            return
        }
    }

    fun getAlarmDataFromPreferences(listener: BasicListener<Alarm>) {
        listener.onStart()
            try {
                preferences.getAlarm()?.let {
                    listener.onSuccess(it)
                } ?: kotlin.run {
                    listener.onSuccess(Alarm())
                }
            } catch (e: Exception) {
                listener.onFailed(e)
            }
    }

    private fun getDatabaseName(): String? {
        return preferences.getDataBaseName()
    }

    private fun getLastDate(): String? {
        return preferences.getLastDate()
    }

    private fun getDatabaseReference(path: String?): DatabaseReference {
        path?.let {
            return getFirebaseReference().getReference(path)
        } ?: run {
            return getFirebaseReference().reference
        }
    }

    private fun getFirebaseReference(): FirebaseDatabase {
        return FirebaseDatabase.getInstance()
    }
}