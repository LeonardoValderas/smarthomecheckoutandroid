package com.lavsystems.smarthomecheckout.data.dataSource

import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

interface AlarmDataSource {
    fun saveAlarm(alarm: Alarm, listener: BasicListener<Boolean>)
    fun activateOrDesactivateAlarm(newStatus: Boolean, listener: BasicListener<Boolean>)
    fun getAlarmDataFromPreferences(listener: BasicListener<Alarm>)
    fun getAlarmData(listener: BasicListener<Alarm>)
}