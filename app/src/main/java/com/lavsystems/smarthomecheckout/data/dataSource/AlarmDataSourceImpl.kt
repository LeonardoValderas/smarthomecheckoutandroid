package com.lavsystems.smarthomecheckout.data.dataSource

import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class AlarmDataSourceImpl(private val firebaseManager: FirebaseManager) : AlarmDataSource {

    override fun activateOrDesactivateAlarm(newStatus: Boolean, listener: BasicListener<Boolean>) {
        return firebaseManager.activateOrDesactivateAlarm(newStatus, listener)
    }

    override fun saveAlarm(alarm: Alarm, listener: BasicListener<Boolean>) {
        return firebaseManager.saveAlarm(alarm, listener)
    }

    override fun getAlarmDataFromPreferences(listener: BasicListener<Alarm>) {
        return firebaseManager.getAlarmDataFromPreferences(listener)
    }

    override fun getAlarmData(listener: BasicListener<Alarm>) {
        return firebaseManager.getAlarmData(listener)
    }
}