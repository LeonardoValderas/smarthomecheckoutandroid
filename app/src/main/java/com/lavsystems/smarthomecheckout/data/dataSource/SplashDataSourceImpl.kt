package com.lavsystems.smarthomecheckout.data.dataSource

import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.utils.listeners.LoginListener

class SplashDataSourceImpl(private val firebaseManager: FirebaseManager) : SplashDataSource {
    override fun baseNameExists(): Boolean {
        return firebaseManager.exists()
    }
}