package com.lavsystems.smarthomecheckout.data.repository

import com.lavsystems.smarthomecheckout.data.dataSource.MainDataSource
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener
import com.lavsystems.smarthomecheckout.utils.listeners.LoginListener
import kotlinx.coroutines.Deferred

class MainRepositoryImpl(private val dataSource: MainDataSource): MainRepository {
    override fun validateLastDateChanged(listener: BasicListener<Boolean>) {
        return dataSource.validateLastDateChanged(listener)
    }

    override fun getComponentsFromFirebase(listener: BasicListener<MutableList<Component>>) {
        return dataSource.getComponentsFromFirebase(listener)
    }

    override fun getComponentsFromLocalbase(listener: BasicListener<MutableList<Component>>) {
        return dataSource.getComponentsFromLocalbase(listener)
    }

    override fun resetEvent(componentName: String, listener: BasicListener<Boolean>) {
        return dataSource.resetEvent(componentName, listener)
    }

    override fun activateOrDesactivate(
        componentName: String,
        newStatus: Int,
        listener: BasicListener<Boolean>
    ) {
        return dataSource.activateOrDesactivate(componentName, newStatus, listener)
    }

    override fun getAlarm(listener: BasicListener<Alarm>) {
        return dataSource.getAlarm(listener)
    }

//    override fun alarmIsActive(listener: BasicListener<Boolean>) {
//        return dataSource.alarmIsActive(listener)
//    }

//    override fun getco(components: MutableList<Component>, listener: BasicListener<Boolean>) {
//        return dataSource.com(components, listener)
//    }
}