package com.lavsystems.smarthomecheckout.data.repository

import com.lavsystems.smarthomecheckout.data.dataSource.LoginDataSource
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class LoginRepositoryImpl(private val dataSource: LoginDataSource): LoginRepository {
    override fun validateCustomerId(id: String, listener: BasicListener<Boolean>) {
        return dataSource.validateCustomerId(id, listener)
    }
}