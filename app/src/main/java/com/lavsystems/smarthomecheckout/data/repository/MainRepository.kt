package com.lavsystems.smarthomecheckout.data.repository

import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

interface MainRepository {
    fun validateLastDateChanged(listener: BasicListener<Boolean>)
    fun getComponentsFromFirebase(listener: BasicListener<MutableList<Component>>)
    fun getComponentsFromLocalbase(listener: BasicListener<MutableList<Component>>)
    fun resetEvent(componentName: String, listener: BasicListener<Boolean>)
    fun activateOrDesactivate(componentName: String, newStatus: Int, listener: BasicListener<Boolean>)
    fun getAlarm(listener: BasicListener<Alarm>)
    //fun alarmIsActive(listener: BasicListener<Boolean>)
    //fun getco(components: MutableList<Component>, listener: BasicListener<Boolean>)
}