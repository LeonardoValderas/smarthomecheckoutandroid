package com.lavsystems.smarthomecheckout.data.repository

import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

interface AlarmRepository {
    fun saveAlarm(alarm: Alarm, listener: BasicListener<Boolean>)
    fun activateOrDesactivateAlarm(newStatus: Boolean, listener: BasicListener<Boolean>)
    fun getAlarmDataFromPreferences(listener: BasicListener<Alarm>)
    fun getAlarmData(listener: BasicListener<Alarm>)
}