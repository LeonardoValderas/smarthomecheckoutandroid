package com.lavsystems.smarthomecheckout.data.dataSource

import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener
import com.lavsystems.smarthomecheckout.utils.listeners.LoginListener

class MainDataSourceImpl(private val firebaseManager: FirebaseManager) : MainDataSource {
    override fun getComponentsFromFirebase(listener: BasicListener<MutableList<Component>>) {
        return firebaseManager.getComponents(false, listener)
    }

    override fun validateLastDateChanged(listener: BasicListener<Boolean>) {
       return firebaseManager.lastDateChanged(listener)
    }

    override fun getComponentsFromLocalbase(listener: BasicListener<MutableList<Component>>) {
        //dao
    }

    override fun resetEvent(componentName: String, listener: BasicListener<Boolean>) {
        return firebaseManager.resetEvent(componentName, listener)
    }

    override fun activateOrDesactivate(
        componentName: String,
        newStatus: Int,
        listener: BasicListener<Boolean>
    ) {
        return firebaseManager.activateOrDesactivate(componentName, newStatus, listener)
    }

    override fun getAlarm(listener: BasicListener<Alarm>) {
        return firebaseManager.getAlarmData(listener)
    }

//    override fun alarmIsActive(listener: BasicListener<Boolean>) {
//        return firebaseManager.alarmIsActive(listener)
//    }

//    override fun com(components: MutableList<Component>, listener: BasicListener<Boolean>) {
//        return firebaseManager.activateOrDesactivateComponents(components, listener)
//    }
}