package com.lavsystems.smarthomecheckout.data.repository


interface SplashRepository {
    fun baseNameExists(): Boolean
}