package com.lavsystems.smarthomecheckout.di.utils

import com.lavsystems.smarthomecheckout.utils.preferences.Preferences
import com.lavsystems.smarthomecheckout.utils.preferences.PreferencesImpl
import org.koin.dsl.module

val utilsModule = module {
    single<Preferences> {
        PreferencesImpl(
            context = get()
        )
    }
}