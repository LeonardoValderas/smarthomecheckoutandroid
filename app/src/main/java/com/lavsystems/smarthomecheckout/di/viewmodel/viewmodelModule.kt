package com.lavsystems.smarthomecheckout.di.viewmodel

import com.lavsystems.smarthomecheckout.mvvm.viewmodel.AlarmViewModel
import com.lavsystems.smarthomecheckout.mvvm.viewmodel.LoginViewModel
import com.lavsystems.smarthomecheckout.mvvm.viewmodel.MainViewModel
import com.lavsystems.smarthomecheckout.mvvm.viewmodel.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        SplashViewModel(repository = get())
    }
    viewModel {
        LoginViewModel(repository = get())
    }
    viewModel {
        MainViewModel(repository = get())
    }
    viewModel {
        AlarmViewModel(repository = get())
    }
}