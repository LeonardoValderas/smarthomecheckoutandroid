package com.lavsystems.smarthomecheckout.di.repository

import com.lavsystems.smarthomecheckout.data.repository.*
import org.koin.dsl.module

val repositoryModule = module {
    single<SplashRepository> {
        SplashRepositoryImpl(dataSource = get())
    }
    single<LoginRepository> {
        LoginRepositoryImpl(dataSource = get())
    }
    single<MainRepository> {
        MainRepositoryImpl(dataSource = get())
    }
    single<AlarmRepository> {
        AlarmRepositoryImpl(dataSource = get())
    }
}