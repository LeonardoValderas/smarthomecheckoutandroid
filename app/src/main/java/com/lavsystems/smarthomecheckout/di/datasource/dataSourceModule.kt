package com.lavsystems.smarthomecheckout.di.datasource

import com.lavsystems.smarthomecheckout.data.dataSource.*
import org.koin.dsl.module

val dataSourceModule = module {
    single<SplashDataSource> {
        SplashDataSourceImpl(firebaseManager = get())
    }
    single<LoginDataSource> {
        LoginDataSourceImpl(firebaseManager = get())
    }
    single<MainDataSource> {
        MainDataSourceImpl(firebaseManager = get())
    }
    single<AlarmDataSource> {
        AlarmDataSourceImpl(firebaseManager = get())
    }
}