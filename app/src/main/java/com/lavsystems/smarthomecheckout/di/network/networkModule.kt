package com.lavsystems.smarthomecheckout.di.network

import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import org.koin.dsl.module

val networkModule = module {
    single {
        FirebaseManager()
    }
}