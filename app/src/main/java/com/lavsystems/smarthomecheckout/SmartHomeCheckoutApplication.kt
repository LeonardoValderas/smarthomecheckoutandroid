package com.lavsystems.smarthomecheckout

//import com.google.android.gms.ads.MobileAds

import android.app.Application
import androidx.work.*
import com.google.android.gms.ads.MobileAds
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.lavsystems.smarthomecheckout.di.datasource.dataSourceModule
import com.lavsystems.smarthomecheckout.di.network.networkModule
import com.lavsystems.smarthomecheckout.di.repository.repositoryModule
import com.lavsystems.smarthomecheckout.di.utils.utilsModule
import com.lavsystems.smarthomecheckout.di.viewmodel.viewModelModule
import com.lavsystems.smarthomecheckout.mvvm.view.service.FirebaseWorkManager
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import java.util.concurrent.TimeUnit


class SmartHomeCheckoutApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@SmartHomeCheckoutApplication)
            modules(
                listOf(
                    networkModule,
                    dataSourceModule,
                    repositoryModule,
                    viewModelModule,
                    utilsModule
                )
            )
        }

        FirebaseApp.initializeApp(this)
        MobileAds.initialize(this)


//        val constraints = Constraints.Builder()
//            .setRequiredNetworkType(NetworkType.CONNECTED)
//            .build()
//
////        val firebaseWorkManager = PeriodicWorkRequestBuilder<FirebaseWorkManager>(1,
////            TimeUnit.DAYS)
////            .build()
//        val firebaseWorkManager =
//            OneTimeWorkRequestBuilder<FirebaseWorkManager>().setConstraints(constraints)
//                .build()
//        WorkManager.getInstance(this).enqueue(firebaseWorkManager)

    }
}