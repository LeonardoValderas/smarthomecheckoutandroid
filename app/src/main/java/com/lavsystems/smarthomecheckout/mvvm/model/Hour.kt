package com.lavsystems.smarthomecheckout.mvvm.model

import java.io.Serializable

data class Hour(val id: Int, val hour: String, val hourInt: Int):
    Serializable {
    constructor() : this(0, "",0)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Hour

        if (id != other.id) return false
        if (hour != other.hour) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + hour.hashCode()
        return result
    }

    override fun toString(): String {
        return hour
    }
}