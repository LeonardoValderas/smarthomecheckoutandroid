package com.lavsystems.smarthomecheckout.mvvm.view.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class AlarmStopBroadcastReceiver : BroadcastReceiver() {
    val firebaseManager = FirebaseManager()
    override fun onReceive(context: Context?, p1: Intent?) {
        //print("AlarmStopBroadcastReceiver pasooooo")
        Toast.makeText(context, "AlarmStopBroadcastReceiver", Toast.LENGTH_SHORT).show()
        unactiveComponents(context)
    }

    private fun unactiveComponents(context: Context?) {
        firebaseManager.getComponentsOnceTime(object : BasicListener<MutableList<Component>> {
            override fun onStart() {
            }

            override fun onSuccess(data: MutableList<Component>) {
                Toast.makeText(context, "AlarmStopBroadcastReceiver unactiveComponents", Toast.LENGTH_SHORT).show()

//                data.filter { c -> (c.event_found == 0 && c.active == 1 && c.type != Component.Type.sensor_gas) }
//                    .let { components ->
//                        if (components.isEmpty()) {
//                            return
//                        }
                data?.let main@ { components ->
                    components.filter { c -> c.active == 1 }.let {
                        if(it.isEmpty() || (it.isNotEmpty() && it.size == 1 && it[0].type == Component.Type.sensor_gas)){
                            return@main
                        }
                    }
                    val newList = mutableListOf<Component>()
                    components.map {
                        val c = if (it.type == Component.Type.sensor_gas)
                            Component(
                                id = it.id,
                                active = 1,
                                event_found = it.event_found,
                                event_time = it.event_time,
                                name = it.name,
                                componentName = it.componentName,
                                type = it.type,
                                min_value = it.min_value,
                                max_value = it.max_value
                            )
                        else
                            Component(
                                id = it.id,
                                active = 0,
                                event_found = it.event_found,
                                event_time = it.event_time,
                                name = it.name,
                                componentName = it.componentName,
                                type = it.type,
                                min_value = it.min_value,
                                max_value = it.max_value
                            )
                        newList.add(
                            c
                        )
                    }

                    firebaseManager.activateOrDesactivateComponents(
                        newList,
                        object : BasicListener<Boolean> {
                            override fun onStart() {
                                Toast.makeText(context, "AlarmStoptBroadcastReceiver onStart", Toast.LENGTH_SHORT).show()
                            }

                            override fun onSuccess(data: Boolean) {
                                Toast.makeText(context, "AlarmStopBroadcastReceiver onSuccess", Toast.LENGTH_SHORT).show()
                            }

                            override fun onFailed(e: Exception) {
                                Toast.makeText(context, "AlarmStopBroadcastReceiver onFailed", Toast.LENGTH_SHORT).show()
                            }
                        })
                }
            }

            override fun onFailed(e: Exception) {
                e.printStackTrace()
            }
        })
    }
}