package com.lavsystems.smarthomecheckout.mvvm.view.activity

import android.content.Intent
import android.os.Build
import android.os.SystemClock
import androidx.lifecycle.Observer
import com.lavsystems.smarthomecheckout.BR
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.base.BaseActivity
import com.lavsystems.smarthomecheckout.databinding.ActivitySplashBinding
import com.lavsystems.smarthomecheckout.mvvm.view.service.FirebaseBackgroundService
import com.lavsystems.smarthomecheckout.mvvm.viewmodel.SplashViewModel
import com.lavsystems.smarthomecheckout.utils.StatusResponse
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {
    private val viewModel: SplashViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_splash
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): SplashViewModel = viewModel
    override fun init() {
        Intent(this, FirebaseBackgroundService::class.java).run {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (startForegroundService(this) == null) {
                    startForegroundService(this)
                    return
                }
            }

            if (startService(this) == null)
                startService(this)
        }

        Thread {
            kotlin.run {
                SystemClock.sleep(2000)
            }
        }.start()
    }

    override fun initObservers() {
        with(getViewModel()) {
            exists.observe(this@SplashActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        data.data?.let {
                            if (it) {
                                goToMainActivity()
                            } else {
                                goToLoginActivity()
                            }
                        }
                        setLoading(false)
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })
        }
    }

    private fun goToMainActivity() {
        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
    }

    private fun goToLoginActivity() {
        startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
    }

    private fun setLoading(loading: Boolean) {
        getRootDataBinding().loading = loading
    }
}