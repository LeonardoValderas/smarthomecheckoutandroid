package com.lavsystems.smarthomecheckout.mvvm.view.listener

import com.lavsystems.smarthomecheckout.mvvm.model.Component

interface OnDialoComponentListener {
  fun updateEvent(component: Component)
  fun updateActive(component: Component)
}
