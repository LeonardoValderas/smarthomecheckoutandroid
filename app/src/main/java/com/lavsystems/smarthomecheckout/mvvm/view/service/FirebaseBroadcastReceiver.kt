package com.lavsystems.smarthomecheckout.mvvm.view.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.Toast

class FirebaseBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == Intent.ACTION_BOOT_COMPLETED) {
            Toast.makeText(context, "Servicio Activo", Toast.LENGTH_SHORT).show()
            Intent(context, FirebaseBackgroundService::class.java).run {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context?.startForegroundService(this)
                    return
                }

                context?.startService(this)
            }
        }
    }
}