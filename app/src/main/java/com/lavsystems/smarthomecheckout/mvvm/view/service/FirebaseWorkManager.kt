package com.lavsystems.smarthomecheckout.mvvm.view.service

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.mvvm.view.notification.Notification
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener
import org.koin.java.KoinJavaComponent

class FirebaseWorkManager(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {
    val firebaseManager by KoinJavaComponent.inject(FirebaseManager::class.java)
    //val preferences by KoinJavaComponent.inject(Preferences::class.java)

    override fun doWork(): Result {
        firebaseManager.getComponents(false, object : BasicListener<MutableList<Component>> {
            override fun onStart() {
            }

            override fun onSuccess(data: MutableList<Component>) {
                data.filter { c -> (c.event_found == 1 && c.active == 1) }.let { components ->
                    if(components.isEmpty()){
                        return
                    }

                    var description = applicationContext.getString(R.string.event_found_notification) + "from workmanager"
                    components.forEach { component ->
                        description += "${component.type.type} -> ${component.name}\n"
                    }
                    showNotification(applicationContext, description)
                }
            }

            override fun onFailed(e: Exception) {
                e.printStackTrace()
            }
        })
        return Result.success()
    }

    fun showNotification(context: Context, description: String) {
       // Notification().notification(context, null, description)
    }
}