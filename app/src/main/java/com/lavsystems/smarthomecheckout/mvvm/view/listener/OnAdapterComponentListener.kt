package com.lavsystems.smarthomecheckout.mvvm.view.listener

import com.lavsystems.smarthomecheckout.mvvm.model.Component

interface OnAdapterComponentListener {
  fun onClick(position: Int, component: Component)
}
