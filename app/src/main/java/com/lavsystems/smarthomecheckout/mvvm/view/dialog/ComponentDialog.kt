package com.lavsystems.smarthomecheckout.mvvm.view.dialog


import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.databinding.DialogLayoutBinding
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.mvvm.view.listener.OnDialoComponentListener
import kotlinx.android.synthetic.main.dialog_layout.*


class ComponentDialog(
    private val component: Component,
    private val listener: OnDialoComponentListener
) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val binding: DialogLayoutBinding = DataBindingUtil.inflate(
                LayoutInflater.from(
                    context
                ), R.layout.dialog_layout, null, false
            )

            binding.model = component
            builder.setView(binding.root)
                .setMessage("${component.type.type}: ${component.name}")
//                .setPositiveButton("Actualizar",
//                    DialogInterface.OnClickListener { dialog, id ->
//                        listener.updateComponent(binding.model!!)
//
//                        dismiss()
//
//                    })
                .setNegativeButton("cancel",
                    DialogInterface.OnClickListener { dialog, id ->
                        dismiss()
                    })
            binding.root.findViewById<Button>(R.id.btn_active).setOnClickListener {
                listener.updateActive(component)
                dismiss()
            }
            binding.root.findViewById<Button>(R.id.btn_reset).setOnClickListener {
                listener.updateEvent(component)
                dismiss()
            }
            builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }
}