package com.lavsystems.smarthomecheckout.mvvm.view.activity

import android.content.Intent
import androidx.lifecycle.Observer
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.BR
import com.lavsystems.smarthomecheckout.base.BaseActivity
import com.lavsystems.smarthomecheckout.databinding.ActivityLoginBinding
import com.lavsystems.smarthomecheckout.mvvm.viewmodel.LoginViewModel
import com.lavsystems.smarthomecheckout.utils.StatusResponse
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.cl_container
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {

    private val viewModel: LoginViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_login
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): LoginViewModel = viewModel

    override fun init() {
        btn_login.setOnClickListener {
            tryLogin()
        }
    }

    override fun initObservers() {
        with(getViewModel()) {
            loginByCustomerId.observe(this@LoginActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        data.data?.let {
                            if (it) {
                                et_code_id.text = null
                                goToMainActivity()
                            } else {
                                showSnackbar(cl_container, getString(R.string.code_incorrect))
                            }
                        }
                        setLoading(false)
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })
        }
    }

    private fun tryLogin() {
        val code = et_code_id.text.toString()
        if (code.isNullOrEmpty()) {
            showSnackbar(cl_container, getString(R.string.login_code_id_empty))
        } else {
            getViewModel().validateCustomerId(code)
        }
    }

    private fun goToMainActivity() {
        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
    }

    private fun setLoading(loading: Boolean) {
        getRootDataBinding().loading = loading
    }
}