package com.lavsystems.smarthomecheckout.mvvm.model

import java.io.Serializable

data class Alarm(val startHour: Hour, val stopHour: Hour, val active: Boolean):
    Serializable {
    constructor(): this(Hour(), Hour(), false)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Alarm

        if (startHour != other.startHour) return false
        if (stopHour != other.stopHour) return false
        if (active != other.active) return false

        return true
    }

    override fun hashCode(): Int {
        var result = startHour.hashCode()
        result = 31 * result + stopHour.hashCode()
        result = 31 * result + active.hashCode()
        return result
    }
}