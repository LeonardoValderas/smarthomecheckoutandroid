package com.lavsystems.smarthomecheckout.mvvm.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.smarthomecheckout.databinding.ComponentItemBinding
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.mvvm.view.listener.OnAdapterComponentListener

class ComponentAdapter(private val listener: OnAdapterComponentListener) :
    RecyclerView.Adapter<ComponentAdapter.ViewHolder>() {

    private var components = mutableListOf<Component>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ComponentItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(components[position], listener)

    override fun getItemCount(): Int = components.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getItemForPosition(position: Int): Component {
        return components[position]
    }

    fun addComponents(components: MutableList<Component>) {
        this.components.clear()
        this.components.addAll(components)
        notifyDataSetChanged()
    }

//    fun deselectAllModels() {
//        divisions.forEach { division -> division.selected = false }
//        notifyDataSetChanged()
//    }
//
//    fun selectModel(position: Int) {
//        divisions[position].selected = true
//        notifyDataSetChanged()
//    }

    class ViewHolder(private var binding: ComponentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(component: Component, listener: OnAdapterComponentListener) {
            with(binding) {
                model = component
                root.setOnClickListener {
                    listener.onClick(layoutPosition, component)
                }
                executePendingBindings()
            }
        }
    }
}