package com.lavsystems.smarthomecheckout.mvvm.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.lavsystems.smarthomecheckout.databinding.HourItemBinding
import com.lavsystems.smarthomecheckout.mvvm.model.Hour

class AlarmDateAdapter constructor(
    context: Context,
    resource: Int,
    private val hours: MutableList<Hour>
) : ArrayAdapter<Hour>(context, resource, hours) {

    override fun getItem(position: Int): Hour? {
        return hours[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return hours.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = HourItemBinding.inflate(inflater, parent, false)
        val hour = getModelForPosition(position)
        bind(binding, hour)
        return binding.root
    }

    fun getModelForPosition(position: Int): Hour {
        return hours[position]
    }

    private fun bind(binding: HourItemBinding, hour: Hour) {
        with(binding) {
            model = hour
            executePendingBindings()
        }
    }

    fun setHours(hours: MutableList<Hour>) {
        this.hours.clear()
        hours.addAll(hours)
        notifyDataSetChanged()
    }
}
