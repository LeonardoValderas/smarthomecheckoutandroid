package com.lavsystems.smarthomecheckout.mvvm.view.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.os.PowerManager
import android.os.SystemClock
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.mvvm.view.notification.Notification
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class FirebaseBackgroundService : Service() {
    private val firebaseManager = FirebaseManager()
    private var isServiceStarted = false
    private var wakeLock: PowerManager.WakeLock? = null

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        firebaseManager.getComponents(false, object : BasicListener<MutableList<Component>> {
            override fun onStart() {
            }

            override fun onSuccess(data: MutableList<Component>) {
                data.filter { c -> (c.event_found == 1 && c.active == 1) }.let { components ->
                    if(components.isEmpty()){
                        return
                    }

                    var description = this@FirebaseBackgroundService.getString(R.string.event_found_notification)
                    components.forEach { component ->
                        description += "${component.type.type} -> ${component.name}\n"
                    }
                    showNotification(this@FirebaseBackgroundService, description)
                }
            }

            override fun onFailed(e: Exception) {
                e.printStackTrace()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        //Toast.makeText(this, "Service destroyed", Toast.LENGTH_SHORT).show()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startService()
        return START_STICKY
    }

    fun showNotification(context: Context, description: String) {
        Notification().show(context, null, description)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        val restartServiceIntent = Intent(applicationContext, FirebaseBackgroundService::class.java).also {
            it.setPackage(packageName)
        };
        val restartServicePendingIntent: PendingIntent = PendingIntent.getService(this, 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT)
        applicationContext.getSystemService(Context.ALARM_SERVICE)
        val alarmService: AlarmManager = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePendingIntent)
    }

    private fun startService() {
        if (isServiceStarted) return
       // Toast.makeText(this, "Service starting its task", Toast.LENGTH_SHORT).show()
        isServiceStarted = true

        // we need this lock so our service gets not affected by Doze Mode
        wakeLock =
            (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "FirebaseBackgroundService::lock").apply {
                    acquire(10*60*1000L /*10 minutes*/)
                }
            }

        Toast.makeText(this, "Service started", Toast.LENGTH_SHORT).show()
    }
}
