package com.lavsystems.smarthomecheckout.mvvm.model

data class Component(
    val id: String,
    val active: Int,
    val event_found: Int,
    val event_time: String,
    val name: String,
    val componentName: String,
    //val componentName: Int,
    val type: Type,
    val min_value: Int?,
    val max_value: Int?
) {
    constructor(): this("", -1, -1, "", "", "", Type.sensor_door, null, null)
    enum class Type(val type: String) {
        sensor_door("Sensor Puertas"),
        sensor_window("Sensor Ventanas"),
        sensor_gas("Sensor Gas"),
        sensor_movement("Sensor Movimiento"),
    }

    val drawableIcon: String
    get() = when(type){
        Type.sensor_door -> {
            "ic_sensor_door"
        }
        Type.sensor_window -> {
            "ic_sensor_window"
        }
        Type.sensor_gas -> {
            "ic_sensor_gas"
        }
        Type.sensor_movement -> {
            "ic_sensor_movement"
        }
    }
}