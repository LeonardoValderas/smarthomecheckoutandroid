package com.lavsystems.smarthomecheckout.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lavsystems.smarthomecheckout.base.BaseViewModel
import com.lavsystems.smarthomecheckout.data.repository.LoginRepository
import com.lavsystems.smarthomecheckout.mvvm.model.DataResponse
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class LoginViewModel(private val repository: LoginRepository): BaseViewModel() {

    private var _loginByCustomerId = MutableLiveData<DataResponse<Boolean>>()
    val loginByCustomerId: LiveData<DataResponse<Boolean>>
        get() = _loginByCustomerId

    fun validateCustomerId(id: String){
            repository.validateCustomerId(id,  object: BasicListener<Boolean> {
                override fun onStart() {
                    _loginByCustomerId.postValue(DataResponse.LOADING())
                }

                override fun onSuccess(data: Boolean) {
                    _loginByCustomerId.postValue(DataResponse.SUCCESS(data))
                }

                override fun onFailed(e: Exception) {
                    _loginByCustomerId.postValue(DataResponse.ERROR(e.message))
                }
            })
    }
}