package com.lavsystems.smarthomecheckout.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lavsystems.smarthomecheckout.base.BaseViewModel
import com.lavsystems.smarthomecheckout.data.repository.MainRepository
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.mvvm.model.DataResponse
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class MainViewModel(private val repository: MainRepository) : BaseViewModel() {

    private var _components = MutableLiveData<DataResponse<MutableList<Component>>>()
    val components: LiveData<DataResponse<MutableList<Component>>>
        get() = _components

    private var _lastDataChanged = MutableLiveData<DataResponse<Boolean>>()
    val lastDataChanged: LiveData<DataResponse<Boolean>>
        get() = _lastDataChanged

    private var _resetEvent = MutableLiveData<DataResponse<Boolean>>()
    val resetEvent: LiveData<DataResponse<Boolean>>
        get() = _resetEvent

    private var _activateOrDesactivate = MutableLiveData<DataResponse<Boolean>>()
    val activateOrDesactivate: LiveData<DataResponse<Boolean>>
        get() = _activateOrDesactivate

    private var _alarmData = MutableLiveData<DataResponse<Alarm>>()
    val alarmData: LiveData<DataResponse<Alarm>>
        get() = _alarmData

    init {
        validateLastDateChanged()
        getAlarm()
    }

    private fun validateLastDateChanged() {
        repository.validateLastDateChanged(object : BasicListener<Boolean> {
            override fun onStart() {
                _lastDataChanged.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: Boolean) {
                _lastDataChanged.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _lastDataChanged.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

    fun getComponentsFromFirebase() {
        repository.getComponentsFromFirebase(object : BasicListener<MutableList<Component>> {
            override fun onStart() {
                _components.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: MutableList<Component>) {
                _components.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _components.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

    fun getComponentsFromLocalbase() {
        repository.getComponentsFromLocalbase(object : BasicListener<MutableList<Component>> {
            override fun onStart() {
                _components.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: MutableList<Component>) {
                _components.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _components.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

    fun resetEvent(componentName: String){
        repository.resetEvent(componentName, object : BasicListener<Boolean> {
            override fun onStart() {
                _resetEvent.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: Boolean) {
                _resetEvent.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _resetEvent.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

    fun activateOrDesactivate(componentName: String, newStatus: Int){
        repository.activateOrDesactivate(componentName, newStatus, object : BasicListener<Boolean> {
            override fun onStart() {
                _activateOrDesactivate.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: Boolean) {
                _activateOrDesactivate.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _activateOrDesactivate.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

    fun getAlarm() {
        repository.getAlarm(object : BasicListener<Alarm> {
            override fun onStart() {
                _alarmData.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: Alarm) {
                _alarmData.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _alarmData.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

//    fun compo(components: MutableList<Component>){
//        repository.getco(components, object : BasicListener<Boolean> {
//            override fun onStart() {
//                //_alarmData.postValue(DataResponse.LOADING())
//            }
//
//            override fun onSuccess(data: Boolean) {
//               // _alarmData.postValue(DataResponse.SUCCESS(data))
//            }
//
//            override fun onFailed(e: Exception) {
//                //_alarmData.postValue(DataResponse.ERROR(e.message))
//            }
//        })
//    }
}