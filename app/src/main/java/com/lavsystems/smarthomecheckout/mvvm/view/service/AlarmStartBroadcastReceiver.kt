package com.lavsystems.smarthomecheckout.mvvm.view.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.lavsystems.smarthomecheckout.data.network.firebase.FirebaseManager
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class AlarmStartBroadcastReceiver : BroadcastReceiver() {
    val firebaseManager = FirebaseManager()
    override fun onReceive(context: Context?, p1: Intent?) {
        //print("AlarmStartBroadcastReceiver pasooooo")
        Toast.makeText(context, "AlarmStartBroadcastReceiver", Toast.LENGTH_SHORT).show()
        activeComponents(context)
    }

    private fun activeComponents(context: Context?) {
        firebaseManager.getComponentsOnceTime(object : BasicListener<MutableList<Component>> {
            override fun onStart() {
            }

            override fun onSuccess(data: MutableList<Component>) {
//                data.filter { c -> (c.active == 0) }.let { components ->
//                    if (components.isEmpty()) {
//                        return
//                    }
                data?.let main@{ components ->
                    components.filter { c -> c.active == 0 }.let {
                        if(it.isEmpty()){
                            return@main
                        }
                    }

                    val newList = mutableListOf<Component>()
                    components.map {
                        newList.add(
                            Component(
                                id = it.id,
                                active = 1,
                                event_found = it.event_found,
                                event_time = it.event_time,
                                name = it.name,
                                componentName = it.componentName,
                                type = it.type,
                                min_value = it.min_value,
                                max_value = it.max_value
                            )
                        )
                    }

                    firebaseManager.activateOrDesactivateComponents(
                        newList,
                        object : BasicListener<Boolean> {
                            override fun onStart() {
                                Toast.makeText(context, "AlarmStartBroadcastReceiver onStart", Toast.LENGTH_SHORT).show()
                            }

                            override fun onSuccess(data: Boolean) {
                                Toast.makeText(context, "AlarmStartBroadcastReceiver onSuccess", Toast.LENGTH_SHORT).show()
                            }

                            override fun onFailed(e: Exception) {
                                Toast.makeText(context, "AlarmStartBroadcastReceiver onFailed", Toast.LENGTH_SHORT).show()
                            }
                        })
                }
            }

            override fun onFailed(e: Exception) {
                e.printStackTrace()
            }
        })
    }
}