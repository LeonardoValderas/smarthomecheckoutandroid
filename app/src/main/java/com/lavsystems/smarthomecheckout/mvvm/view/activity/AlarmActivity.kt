package com.lavsystems.smarthomecheckout.mvvm.view.activity

import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.Observer
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.BR
import com.lavsystems.smarthomecheckout.base.BaseActivity
import com.lavsystems.smarthomecheckout.databinding.ActivityAlarmBinding
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.model.Hour
import com.lavsystems.smarthomecheckout.mvvm.view.adapter.AlarmDateAdapter
import com.lavsystems.smarthomecheckout.mvvm.viewmodel.AlarmViewModel
import com.lavsystems.smarthomecheckout.utils.AlarmUtils
import com.lavsystems.smarthomecheckout.utils.StatusResponse
import kotlinx.android.synthetic.main.activity_alarm.*
import kotlinx.android.synthetic.main.activity_main.adView
import kotlinx.android.synthetic.main.activity_main.cl_container
import kotlinx.android.synthetic.main.activity_main.toolbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class AlarmActivity : BaseActivity<ActivityAlarmBinding, AlarmViewModel>() {
    private val viewModel: AlarmViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_alarm
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): AlarmViewModel = viewModel

    private lateinit var startAdapter: AlarmDateAdapter
    private lateinit var stopAdapter: AlarmDateAdapter

    private var startHour = Hour()
    private var stopHour = Hour()

    override fun init() {
        initToolbar()
        initAdMob()
        initButtons()
        initSpinnerAdapter()
        initSpinners()
    }

    override fun initObservers() {
        with(getViewModel()) {
            alarmData.observe(this@AlarmActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        data.data?.let {
                            if (it.active) {
                                initBroadcastAlarm(it)
                            } else {
                                cancelBroadcastAlarm()
                            }
                            setModelToLayout(alarm = it)
                        } ?: run {
                            closeActivity()
                        }
                        setLoading(false)
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        closeActivity()
                    }
                }
            })

            saved.observe(this@AlarmActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        setLoading(false)
                        getAlarmData()
                        showSnackbar(cl_container, getString(R.string.alarm_saved))
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        //show error icon
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })

            activateOrDesactivateAlarm.observe(this@AlarmActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        setLoading(false)
                        getAlarmData()
                        showSnackbar(cl_container, getString(R.string.alarm_event_success))
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        //show error icon
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })
        }
    }

    fun initBroadcastAlarm(alarm: Alarm) {
        cancelBroadcastAlarm()
        AlarmUtils.initAlarm(alarm, this)
    }

    fun cancelBroadcastAlarm() {
        AlarmUtils.cancelAlarm(this)
    }

    private fun initSpinnerAdapter() {
        startAdapter = AlarmDateAdapter(this, R.layout.simple_item_spinner, getHours())
        stopAdapter = AlarmDateAdapter(this, R.layout.simple_item_spinner, getHours())
        sp_start?.adapter = startAdapter
        sp_stop?.adapter = stopAdapter
    }

    private fun initSpinners() {
        sp_start?.apply {
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    startAdapter.getModelForPosition(position)?.let {
                        startHour = it
                    }

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
            }
        }

        sp_stop?.apply {
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    stopAdapter.getModelForPosition(position)?.let {
                        stopHour = it
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {

                }
            }
        }
    }

    private fun getHours(): MutableList<Hour> {
        return mutableListOf(
            Hour(1, "1 AM", 1),
            Hour(2, "2 AM", 2),
            Hour(3, "3 AM", 3),
            Hour(4, "4 AM", 4),
            Hour(5, "5 AM", 5),
            Hour(6, "6 AM", 6),
            Hour(7, "7 AM", 7),
            Hour(8, "8 AM", 8),
            Hour(9, "9 AM", 9),
            Hour(10, "10 AM", 10),
            Hour(11, "11 AM", 11),
            Hour(12, "12 PM", 12),
            Hour(14, "13 PM", 13),
            Hour(15, "14 PM", 14),
            Hour(16, "15 PM", 15),
            Hour(17, "16 PM", 16),
            Hour(18, "17 PM", 17),
            Hour(19, "18 PM", 18),
            Hour(20, "19 PM", 19),
            Hour(21, "20 PM", 20),
            Hour(22, "21 PM", 21),
            Hour(23, "22 PM", 22),
            Hour(24, "23 PM", 23),
            Hour(25, "00 AM", 0)
        )
    }

    private fun closeActivity() {
        showSnackbar(cl_container, getString(R.string.known_error))
        finish()
    }

    private fun initToolbar() {
        toolbar.title = getString(R.string.alarm_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initAdMob() {
        MobileAds.initialize(this, getString(R.string.app_admob_id));
        AdRequest.Builder().build().also {
            adView?.loadAd(it)
        }
    }

    private fun setLoading(loading: Boolean) {
        getRootDataBinding().loading = loading
    }

    private fun initButtons() {
        btn_save.setOnClickListener {
            if (startHour == stopHour) {
                showSnackbar(cl_container, getString(R.string.equals_hours_error))
                return@setOnClickListener
            }
            getRootDataBinding().model?.let {
                getViewModel().saveAlarm(Alarm(startHour, stopHour, it.active))
            } ?: kotlin.run {
                showSnackbar(cl_container, getString(R.string.known_error))
            }
        }

        btn_active.setOnClickListener {
            getRootDataBinding().model?.let {
                getViewModel().activateOrDesactivateAlarm(!it.active)
            } ?: kotlin.run {
                showSnackbar(cl_container, getString(R.string.known_error))
            }
        }
    }

    private fun setModelToLayout(alarm: Alarm) {
        sp_start.setSelection(startAdapter.getPosition(alarm.startHour))
        sp_stop.setSelection(stopAdapter.getPosition(alarm.stopHour))
        getRootDataBinding().model = alarm
    }
}