package com.lavsystems.smarthomecheckout.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.lavsystems.smarthomecheckout.base.BaseViewModel
import com.lavsystems.smarthomecheckout.data.repository.MainRepository
import com.lavsystems.smarthomecheckout.data.repository.SplashRepository
import com.lavsystems.smarthomecheckout.mvvm.model.DataResponse
import com.lavsystems.smarthomecheckout.utils.StatusResponse
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener
import com.lavsystems.smarthomecheckout.utils.listeners.LoginListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.Exception

class SplashViewModel(private val repository: SplashRepository): BaseViewModel() {

    private var _exists = MutableLiveData<DataResponse<Boolean>>()
    val exists: LiveData<DataResponse<Boolean>>
        get() = _exists

    init {
        baseNameExists()
    }

    private fun baseNameExists() {
        _exists.postValue(DataResponse.LOADING())
        try {
            val exists = repository.baseNameExists()
            _exists.postValue(DataResponse.SUCCESS(exists))
        }catch (e: Exception){
            _exists.postValue(DataResponse.ERROR(e.message))
        }
    }
}