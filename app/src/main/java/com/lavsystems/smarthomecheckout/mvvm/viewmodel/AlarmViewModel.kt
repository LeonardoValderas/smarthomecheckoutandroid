package com.lavsystems.smarthomecheckout.mvvm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lavsystems.smarthomecheckout.base.BaseViewModel
import com.lavsystems.smarthomecheckout.data.repository.AlarmRepository
import com.lavsystems.smarthomecheckout.mvvm.model.Alarm
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.mvvm.model.DataResponse
import com.lavsystems.smarthomecheckout.utils.listeners.BasicListener

class AlarmViewModel(private val repository: AlarmRepository) : BaseViewModel() {

    private var _saved = MutableLiveData<DataResponse<Boolean>>()
    val saved: LiveData<DataResponse<Boolean>>
        get() = _saved

    private var _activateOrDesactivateAlarm = MutableLiveData<DataResponse<Boolean>>()
    val activateOrDesactivateAlarm: LiveData<DataResponse<Boolean>>
        get() = _activateOrDesactivateAlarm

    private var _alarmData = MutableLiveData<DataResponse<Alarm>>()
    val alarmData: LiveData<DataResponse<Alarm>>
        get() = _alarmData

    init {
        getAlarmData()
    }

    fun saveAlarm(alarm: Alarm){
        repository.saveAlarm(alarm, object : BasicListener<Boolean> {
            override fun onStart() {
                _saved.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: Boolean) {
                _saved.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _saved.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

    fun activateOrDesactivateAlarm(newStatus: Boolean){
        repository.activateOrDesactivateAlarm( newStatus, object : BasicListener<Boolean> {
            override fun onStart() {
                _activateOrDesactivateAlarm.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: Boolean) {
                _activateOrDesactivateAlarm.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _activateOrDesactivateAlarm.postValue(DataResponse.ERROR(e.message))
            }
        })
    }

    fun getAlarmData() {
        repository.getAlarmData(object : BasicListener<Alarm> {
            override fun onStart() {
                _alarmData.postValue(DataResponse.LOADING())
            }

            override fun onSuccess(data: Alarm) {
                _alarmData.postValue(DataResponse.SUCCESS(data))
            }

            override fun onFailed(e: Exception) {
                _alarmData.postValue(DataResponse.ERROR(e.message))
            }
        })
    }
}