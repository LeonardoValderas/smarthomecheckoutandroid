package com.lavsystems.smarthomecheckout.mvvm.view.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.mvvm.view.activity.MainActivity
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.CHANNEL_ID
import com.lavsystems.smarthomecheckout.utils.ConstantsUtils.CHANNEL_ID_NAME


class Notification {
    fun show(
        context: Context,
        title: String? = context.getString(R.string.notification_title),
        description: String
    ) {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        //val vibrate = longArrayOf(0, 100, 200, 300)
        val vibrate = longArrayOf(500, 500, 500, 500, 500, 500, 500, 500, 500)
        //val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
        val alarmSoundRaw = Uri.parse(
            ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + context.packageName + "/" + R.raw.allusive
        )
        val builder =
             NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notification_alert)
            .setContentTitle(context.getString(R.string.notification_title))
            .setContentText(description)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setVibrate(vibrate)
            //.setSound(alarmSoundRaw, AudioManager.STREAM_NOTIFICATION)
            //.setSound(alarmSoundRaw)
            .setLights(Color.BLUE, 500, 500)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText(description)
                    .setBigContentTitle(title)
            )



        val notification =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val att = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build()
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_ID_NAME,
                NotificationManager.IMPORTANCE_HIGH

            ).also {
                //it.setDescription(msg);
                it.enableLights(true)
                it.enableVibration(true)
                it.vibrationPattern = vibrate
                //it.setSound(alarmSoundRaw, att)
            }
            notification.createNotificationChannel(channel)
            builder.setChannelId(CHANNEL_ID);
        }
        val mp: MediaPlayer = MediaPlayer.create(context, R.raw.telephone_busy_signal)
        mp.start()
        notification.notify(0, getNotification(context, title, description))
    }


    fun getNotification(
        context: Context,
        title: String? = context.getString(R.string.notification_title),
        description: String
    ): android.app.Notification {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val vibrate = longArrayOf(500, 500, 500, 500, 500, 500, 500, 500, 500)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notification =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//            val att = AudioAttributes.Builder()
//                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
//                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
//                .build()
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_ID_NAME,
                NotificationManager.IMPORTANCE_HIGH

            ).also {
                //it.setDescription(msg);
                it.enableLights(true)
                it.enableVibration(true)
                it.vibrationPattern = vibrate
                //it.setSound(alarmSoundRaw, att)
            }
            notification.createNotificationChannel(channel)
            //builder.setChannelId(CHANNEL_ID);
        }

        val builder =
            NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification_alert)
                .setContentTitle(context.getString(R.string.notification_title))
                .setContentText(description)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setVibrate(vibrate)
                //.setSound(alarmSoundRaw, AudioManager.STREAM_NOTIFICATION)
                //.setSound(alarmSoundRaw)
                .setLights(Color.BLUE, 500, 500)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(
                    NotificationCompat.BigTextStyle()
                        .bigText(description)
                        .setBigContentTitle(title)
                )
        val mp: MediaPlayer = MediaPlayer.create(context, R.raw.telephone_busy_signal)
        mp.start()
        return builder.build()
        //notification.notify(0, builder.build())
    }
}
