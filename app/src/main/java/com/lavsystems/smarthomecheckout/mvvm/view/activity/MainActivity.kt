package com.lavsystems.smarthomecheckout.mvvm.view.activity

import android.content.Intent
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.lavsystems.smarthomecheckout.BR
import com.lavsystems.smarthomecheckout.R
import com.lavsystems.smarthomecheckout.base.BaseActivity
import com.lavsystems.smarthomecheckout.databinding.ActivityMainBinding
import com.lavsystems.smarthomecheckout.mvvm.model.Component
import com.lavsystems.smarthomecheckout.mvvm.view.adapter.ComponentAdapter
import com.lavsystems.smarthomecheckout.mvvm.view.dialog.ComponentDialog
import com.lavsystems.smarthomecheckout.mvvm.view.listener.OnAdapterComponentListener
import com.lavsystems.smarthomecheckout.mvvm.view.listener.OnDialoComponentListener
import com.lavsystems.smarthomecheckout.mvvm.viewmodel.MainViewModel
import com.lavsystems.smarthomecheckout.utils.AlarmUtils
import com.lavsystems.smarthomecheckout.utils.StatusResponse
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), OnAdapterComponentListener,
    OnDialoComponentListener {
    private val viewModel: MainViewModel by viewModel()
    override fun layoutRes(): Int = R.layout.activity_main
    override fun getBindingVariable(): Int = BR.vm
    override fun initViewModel(): MainViewModel = viewModel

    private val componentAdapter = ComponentAdapter(this)

    override fun init() {
        initToolbar()
        recyclerView()
        initAdMob()
    }

    override fun initObservers() {
        with(getViewModel()) {
            lastDataChanged.observe(this@MainActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        data.data?.let { changed ->
                            if (changed) {
                                getComponentsFromFirebase()
                            } else {
                                //getComponentsFromLocalbase()
                                getComponentsFromFirebase()
                            }
                        }
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        //show error icon
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })

//            alarmData.observe(this@MainActivity, Observer { data ->
//                when (data.status) {
//                    StatusResponse.LOADING -> {
//                        setLoading(true)
//                    }
//                    StatusResponse.SUCCESS -> {
//                        data.data?.let { alarm ->
//                            if (alarm.active) {
//                                AlarmUtils.cancelAlarm(this@MainActivity)
//                                AlarmUtils.initAlarm(alarm,this@MainActivity)
//                            }
//                        }
//                    }
//                    StatusResponse.ERROR -> {
//                        setLoading(false)
//                    }
//                }
//            })

            components.observe(this@MainActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        data.data?.let { components ->
                            componentAdapter.addComponents(components)
                        }
                        setLoading(false)
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })

            resetEvent.observe(this@MainActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        setLoading(false)
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        //show error icon
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })

            activateOrDesactivate.observe(this@MainActivity, Observer { data ->
                when (data.status) {
                    StatusResponse.LOADING -> {
                        setLoading(true)
                    }
                    StatusResponse.SUCCESS -> {
                        setLoading(false)
                    }
                    StatusResponse.ERROR -> {
                        setLoading(false)
                        //show error icon
                        showSnackbar(cl_container, data.message ?: getString(R.string.known_error))
                    }
                }
            })
        }
    }

    private fun initToolbar() {
        toolbar.title = getString(R.string.my_sensors)
        setSupportActionBar(toolbar)
    }

    private fun recyclerView() {
        rv_component.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@MainActivity, 2);
            adapter = componentAdapter
        }
    }

    private fun initAdMob() {
        MobileAds.initialize(this, getString(R.string.app_admob_id));
        AdRequest.Builder().build().also {
            adView?.loadAd(it)
        }
    }

    private fun setLoading(loading: Boolean) {
        getRootDataBinding().loading = loading
    }

    override fun onClick(position: Int, component: Component) {
        ComponentDialog(component, this).show(supportFragmentManager, "dialog_component")
    }

    override fun updateEvent(component: Component) {
        (supportFragmentManager.findFragmentByTag("dialog_component") as? DialogFragment)?.dismiss()
        getViewModel().resetEvent(component.componentName)
    }

    override fun updateActive(component: Component) {
        (supportFragmentManager.findFragmentByTag("dialog_component") as? DialogFragment)?.dismiss()
        getViewModel().activateOrDesactivate(
            component.componentName,
            if (component.active == 1) 0 else 1
        )
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.alarm -> {
                startActivity(Intent(this, AlarmActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}