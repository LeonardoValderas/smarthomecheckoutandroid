package com.lavsystems.smarthomecheckout.base

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {
    private var viewDataBinding: T? = null
    private var viewModel: V? = null

    @LayoutRes
    protected abstract fun layoutRes(): Int

    protected abstract fun getBindingVariable(): Int
    protected abstract fun initViewModel(): V
    protected abstract fun initObservers()
    protected abstract fun init()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        init()
        initViewModel()
        initObservers()
    }

    fun getRootDataBinding(): T {
        return viewDataBinding!!
    }

    fun getViewModel() = this.viewModel.let {
        when (it) {
            null -> initViewModel()
            else -> it
        }
    }

    private fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutRes())
        viewDataBinding!!.setVariable(getBindingVariable(), getViewModel())
        viewDataBinding!!.executePendingBindings()
    }

//    protected fun showMainLayout(idLayout: Int) {
//        val view = findViewById<View>(idLayout)
//        view?.visibility = View.VISIBLE
//    }
//
//    protected fun hideMainLayout(idLayout: Int) {
//        val view = findViewById<View>(idLayout)
//        view?.visibility = View.GONE
//    }
//
//    protected fun setImageViewFriendlyError(reource: Int){
//       // Glide.with(this).load(reource).into(iv_error)
//    }
//
//    protected fun setTitleFriendlyError(title: String){
//        //tv_title_error?.text = title
//    }
//
//    protected fun setSubTitleFriendlyError(subtitle: String){
//       // tv_subtitle_error?.text = subtitle
//    }

    fun showSnackbar(view: View, message: String) {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        val view = snackbar.view
        val tv = view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        tv.setTextColor(ContextCompat.getColor(view.context, android.R.color.white))
        snackbar.show()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}