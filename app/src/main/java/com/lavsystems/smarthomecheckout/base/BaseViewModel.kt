package com.lavsystems.smarthomecheckout.base

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.databinding.ViewDataBinding

abstract class BaseViewModel() : ViewModel() {

//    private val _showLoading = MutableLiveData<Boolean>()
//    val showLoading: LiveData<Boolean>
//        get() = _showLoading
//
//    private val _resourceInfoLoading = MutableLiveData<Int>()
//    val resourceInfoLoading: LiveData<Int>
//        get() = _resourceInfoLoading
//
//    private val _showLoadingMore = MutableLiveData<Boolean>()
//    val showLoadingMore: LiveData<Boolean>
//        get() = _showLoadingMore
//
//    private var bundle: Bundle? = null
//
//    protected fun showLoading(resource: Int) {
//        _resourceInfoLoading.value = resource
//        _showLoading.value = true
//    }
//
//    protected fun hideLoading() {
//        _showLoading.value = false
//        _showLoadingMore.value = false
//    }
//
//    protected fun showLoadingMore() {
//        _showLoadingMore.value = true
//    }
//
//    protected fun hideLoadingMore() {
//        _showLoadingMore.value = false
//    }
//
//    protected fun exceptionToLog(e: Throwable) {
//        // is for firebase analytic
//        // Log.e(MY_CITYS_SPORTS_LOG, e.message, e)
//    }
//
//    protected fun simpleErrorLog(error: String) {
//        // is for firebase analytic
//        // Log.e(MY_CITYS_SPORTS_LOG, error)
//    }
//
//    protected fun infoLog(info: String) {
//        // is for firebase analytic
//        // get idUser
//        // get city
//        //institution
//        //  Log.i(MY_CITYS_SPORTS_LOG, info)
//    }
}